<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ------------------ VUE COMPOSER -------------------------- */

View::composer('pages.menu', function($view){
  $view->with('pages', App\Http\Models\Page::all());
});

/* ---------------------------------------------------------  */

/* ------------------ ROUTE VOYAGER -------------------------- */

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

/* ---------------------------------------------------------  */

/* ------------------ ROUTE DES PAGES -------------------------- */

/*
  ROUTE PAR DEFAUT
  PATTERN: /
  CTRL: Pages
  ACTION: show
 */


Route::get('/', 'PagesController@show')
     ->name('pages.show');

 /*
   DÉTAILS D'UNE PAGE
   PATTERN: pages/id/slug.html
   CTRL: Pages
   ACTION: show
  */

Route::get('pages/{id}/{slug}.html', 'PagesController@show')
        ->where([ 'id'   => '[1-9][0-9]*',
                  'slug' => '[a-z0-9][a-z0-9\-]*'])
        ->name('pages.show');

/* ---------------------------------------------------------  */

/* ------------------ ROUTE DES PROJETS -------------------------- */


/*
  DÉTAILS D'UN PROJET
  PATTERN: projets/id/slug.html
  CTRL: Projets
  ACTION: show
 */

 Route::get('projets/{id}/{slug}.html', 'ProjetsController@show')
         ->where([ 'id'   => '[1-9][0-9]*',
                   'slug' => '[a-z0-9][a-z0-9\-]*'])
         ->name('projets.show');


/* ------------------ ROUTE DES ARTICLES -------------------------- */

/*
  DÉTAILS D'UN ARTICLE
  PATTERN: articles/id/slug.html
  CTRL: Articles
  ACTION: show
 */

 Route::get('articles/{id}/{slug}.html', 'ArticlesController@show')
         ->where([ 'id'   => '[1-9][0-9]*',
                   'slug' => '[a-z0-9][a-z0-9\-]*'])
         ->name('articles.show');


//------------------------------------------------------------------------------

//                          -------------------
//                            Route des Tags
//                          -------------------

/*
  DÉTAILS D'UN Tag
  PATTERN: tags/id/slug.html
  CTRL: Tags
  ACTION: show
*/

 Route::get('tags/{id}/{slug}.html', 'TagsController@show')
         ->where([ 'id'   => '[1-9][0-9]*',
                   'slug' => '[a-z0-9][a-z0-9\-]*'])
         ->name('tags.show');
