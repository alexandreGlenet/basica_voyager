-- MySQL dump 10.13  Distrib 5.6.34, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: basica_voyager
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vedette` tinyint(4) DEFAULT NULL,
  `titre_secondaire` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rubrique_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Laravel – Voyager : À consommer avec modération ?','<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">P&rsquo;tit article pour commencer une suite sur un sujet qui nous concerne tous, d&eacute;veloppeurs, l&rsquo;optimisation de notre temps de travail.</span><span style=\"box-sizing: border-box;\">En effet, hors cas exceptionnels, lorsque l&rsquo;on fait un choix de techno qui d&eacute;bouche sur du&nbsp;<a style=\"box-sizing: border-box; background: 0px 0px; text-decoration-line: none; text-rendering: optimizelegibility; color: #0b7fd9; transition: all 0.4s ease 0s;\" href=\"https://www.troispointzero.fr/articles/developper-vos-applications-php-avec-le-sourire-grace-a-laravel/\">Laravel</a>, c&rsquo;est pour des projets qui demandent ou demanderont des fonctionnalit&eacute;s complexes.</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Malgr&eacute; tout, on retrouve toujours certaines &eacute;tapes : par exemple dans le parcours utilisateur avec une phase de connexion / inscription et la plupart du temps un back-office.</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">C&rsquo;est pourquoi chez TroisPointZ&eacute;ro, nous avons test&eacute; (et nous continuerons de tester) plusieurs solutions pour &agrave; la fois gagner du temps sur des &eacute;l&eacute;ments r&eacute;p&eacute;titifs mais pour permettre aussi &agrave; chaque dev de pouvoir comprendre / reprendre le code de son coll&egrave;gue sans perdre 10 jours &agrave; appr&eacute;hender l&rsquo;arborescence du Laravel mise en place.</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Vous l&rsquo;aurez compris, le but de cette s&eacute;rie d&rsquo;articles sera de vous faire d&eacute;couvrir les packages / solutions que nous avons test&eacute; dans un cadre d&rsquo;optimisation et de d&eacute;finition d&rsquo;une base de d&eacute;veloppement solide.</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Pour ce premier article, je vais vous pr&eacute;senter&nbsp;<a style=\"box-sizing: border-box; background: 0px 0px; text-decoration-line: none; text-rendering: optimizelegibility; color: #0b7fd9; transition: all 0.4s ease 0s;\" href=\"https://laravelvoyager.com/\">Laravel Voyager</a>, qui est le premier package que l&rsquo;on a utilis&eacute; dans un but d&rsquo;optimisation, ainsi que le cadre du projet et enfin les c&ocirc;t&eacute;s &lsquo;black and white&rsquo; du package. A savoir que cet article n&rsquo;a pas vocation &agrave; dire si le package est bien ou non &ndash; chacun se fera son avis &ndash; mais de vous aiguiller sur son utilisation.&nbsp;&nbsp;</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<h2 style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px;\">&nbsp;</h2>\r\n<h3>Pourquoi Voyager ?</h3>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><img class=\"alignleft wp-image-918 size-medium\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; height: auto; max-width: 100%; float: left; margin: 0px 2em 1em 0px;\" src=\"https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-300x142.png\" sizes=\"(max-width: 300px) 100vw, 300px\" srcset=\"https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-300x142.png 300w, https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-768x364.png 768w, https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-1024x485.png 1024w, https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-480x228.png 480w, https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-385x182.png 385w, https://www.troispointzero.fr/wp-content/uploads/2018/04/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f746865636f6e74726f6c67726f75702f766f79616765722e706e67-70x33.png 70w\" alt=\"voyager tuto\" width=\"300\" height=\"142\" /></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Pour r&eacute;pondre &agrave; cette question, je me dois de vous expliquer (tr&egrave;s) rapidement le cadre du projet : projet d&rsquo;application mobile d&rsquo;o&ugrave; un besoin de g&eacute;n&eacute;rer des APIs avec une part complexe c&ocirc;t&eacute; m&eacute;tiers (beaucoup de tables, de relations, multilingue, &hellip;) et gestion compl&egrave;te ou presque des donn&eacute;es par les admins (Back office complexe).</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Notre choix s&rsquo;est donc logiquement tourn&eacute; vers&nbsp;<a style=\"box-sizing: border-box; background: 0px 0px; text-decoration-line: none; text-rendering: optimizelegibility; color: #0b7fd9; transition: all 0.4s ease 0s;\" href=\"https://www.troispointzero.fr/articles/developper-vos-applications-php-avec-le-sourire-grace-a-laravel/\">Laravel</a>, ce qui tombait tr&egrave;s bien car nous avions Voyager dans le viseur.</span></p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px 0px 10px; color: #828a94; font-family: \'Open Sans\', sans-serif; font-size: 15px;\">&nbsp;</p>','articles\\February2020\\wp7HdBR9Y5XhHYWs7OzV.png','2020-02-04 15:32:00','2020-02-08 10:00:57',0,'Laravel : Les solutions pour construire une base de travail commune et évolutive ?',3),(2,'Pourquoi Adobe XD devient l’outil préféré de nos Designers ?','<p>Dans un univers o&ugrave; les technologies et les usages &eacute;voluent plus vite qu&rsquo;un Pok&eacute;mon, les m&eacute;tiers li&eacute;s &agrave; la conception se doivent de, sans-cesse, se remettre en question et de se r&eacute;inventer. Il n&rsquo;y a pas si longtemps, au sein de la conception digitale, on faisait appel &agrave; un designer ou un directeur artistique, pour faire un travail de &laquo; peinture et de fa&ccedil;ade &raquo; sur un produit d&eacute;j&agrave; con&ccedil;u (#plusjamaisca). Aujourd&rsquo;hui nous cherchons d&eacute;sormais, un UX ou Product designer pour travailler en amont du projet.</p>','articles\\February2020\\1UldYYaFPf5ffReVyhpV.jpg','2020-02-04 18:23:00','2020-02-08 10:00:39',0,NULL,4),(3,'Construire un SPA Vue avec Laravel','<p>La cr&eacute;ation d\'une application monopage Vue (SPA) avec Laravel est une belle combinaison pour cr&eacute;er des applications propres bas&eacute;es sur l\'API. Dans ce tutoriel, nous vous montrons comment &ecirc;tre op&eacute;rationnel avec un routeur Vue et un backend Laravel pour la construction d\'un SPA. Nous nous concentrerons sur le c&acirc;blage de toutes les pi&egrave;ces n&eacute;cessaires, puis dans un didacticiel de suivi, nous d&eacute;montrerons davantage l\'utilisation de Laravel comme couche API. Le flux principal de fonctionnement d\'un SPA Vue avec Laravel en tant que backend est le suivant: La premi&egrave;re requ&ecirc;te frappe le routeur Laravel c&ocirc;t&eacute; serveur Laravel rend la disposition SPA Les demandes suivantes exploitent l\' history.pushStateAPI pour la navigation URL sans rechargement de page Le routeur Vue peut &ecirc;tre configur&eacute; pour utiliser le mode historique ou le mode de hachage par d&eacute;faut, qui utilise le hachage d\'URL pour simuler une URL compl&egrave;te afin que la page ne se recharge pas lorsque l\'URL change. Nous utiliserons le mode historique, ce qui signifie que nous devons configurer un itin&eacute;raire Laravel qui correspondra &agrave; toutes les URL possibles en fonction de l\'itin&eacute;raire par lequel l\'utilisateur entre dans le SPA Vue. Par exemple, si l\'utilisateur actualise un /helloitin&eacute;raire, nous devons faire correspondre cet itin&eacute;raire et renvoyer le mod&egrave;le d\'application Vue SPA. Le routeur Vue d&eacute;terminera alors l\'itin&eacute;raire et restituera le composant appropri&eacute;.</p>','articles\\February2020\\ACVKquJqzwbfRG0VeSu0.png','2020-02-04 18:41:00','2020-02-08 10:00:23',0,NULL,5),(4,'Comprendre Go en 5 minutes','<p>J&rsquo;ai pass&eacute; du temps avec Go pour m&rsquo;amuser ces derniers mois. Je te propose une introduction au langage pr&eacute;f&eacute;r&eacute; de Google. J&rsquo;ai beaucoup aim&eacute; pour plein de raisons et aujourd&rsquo;hui je t&rsquo;en parle pendant 5 minutes.</p>','articles\\February2020\\KT7sYIuJsV4rOSNDAUPi.jpg','2020-02-04 18:48:00','2020-02-08 10:00:08',0,NULL,1),(5,'Qu\'est-ce que Flutter ?','<p>Flutter est le nouveau Framework de Google pour la cr&eacute;ation d\'application mobile multiplatforme. A l\'heure o&ugrave; cet article est r&eacute;dig&eacute; le Framework est &agrave; la version 1.0. L\'avantage de ce genre de technologie est de pouvoir d&eacute;velopper son application une seule fois pour ensuite la d&eacute;ployer sur le PlayStore mais &eacute;galement sur L\'AppStore. Flutter n\'est pas le premier Framework &agrave; mettre un place un syst&egrave;me de d&eacute;veloppement multiplateforme, nous avons par exemple React Native ou Ionic. Sur le site de Flutter (www.flutter.io) les ing&eacute;nieurs nous assurent qu\'avec ce Framework nous obtiendrons exactement les m&ecirc;mes performances qu\'avec du code natif (swift pour Apple et Koltin pour Android).</p>','articles\\February2020\\bFSiO2KKwNOmpEe1zBX3.png','2020-02-04 18:53:00','2020-02-08 09:59:55',0,NULL,1);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2020-02-01 06:35:18','2020-02-01 06:35:18'),(2,NULL,1,'Category 2','category-2','2020-02-01 06:35:18','2020-02-01 06:35:18');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),(22,4,'id','number','ID',1,0,0,0,0,0,NULL,1),(23,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(24,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),(25,4,'name','text','Name',1,1,1,1,1,1,NULL,4),(26,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(27,4,'created_at','timestamp','Created At',0,0,1,0,0,0,NULL,6),(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(29,5,'id','number','ID',1,0,0,0,0,0,'{}',1),(30,5,'author_id','text','Author',1,0,1,1,0,1,'{}',2),(31,5,'category_id','text','Category',0,0,1,1,1,0,'{}',3),(36,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(40,5,'created_at','timestamp','Created At',0,1,1,0,0,0,'{}',12),(41,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',13),(43,5,'featured','checkbox','Featured',1,1,1,1,1,1,'{}',15),(44,6,'id','number','ID',1,0,0,0,0,0,'{}',1),(45,6,'author_id','text','Author',1,0,0,0,0,0,'{}',2),(49,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),(53,6,'created_at','timestamp','Created At',0,1,1,0,0,0,'{}',10),(54,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',11),(58,6,'description','text_area','Description',0,1,1,1,1,1,'{}',15),(59,6,'titre_menu','text','Titre Menu',1,1,1,1,1,1,'{}',7),(60,6,'titre','text','Titre',1,1,1,1,1,1,'{}',8),(61,6,'titre_description','text','Titre Description',0,1,1,1,1,1,'{}',9),(62,6,'sous_titre_description','text','Sous Titre Description',0,1,1,1,1,1,'{}',10),(63,6,'photo','image','Photo',0,1,1,1,1,1,'{}',11),(64,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(65,8,'titre','text','Titre',1,1,1,1,1,1,'{}',2),(66,8,'photo','image','Photo',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"350\",\"height\":\"250\"}}]}',3),(67,8,'client','text','Client',0,1,1,1,1,1,'{}',4),(68,8,'description','text','Description',0,1,1,1,1,1,'{}',5),(69,8,'date','text','Date',0,1,1,1,1,1,'{}',6),(70,8,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',7),(71,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(72,8,'featured','checkbox','Featured',0,1,1,1,1,1,'{}',9),(78,5,'title','text','Title',1,1,1,1,1,1,'{}',4),(79,5,'excerpt','text','Excerpt',0,1,1,1,1,1,'{}',5),(80,5,'image','text','Image',0,1,1,1,1,1,'{}',6),(81,5,'body','text','Body',0,1,1,1,1,1,'{}',11),(82,9,'id','text','Id',1,0,0,0,0,0,'{}',1),(87,9,'photo','image','Photo',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"350\",\"height\":\"250\"}}]}',6),(88,9,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',8),(89,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',9),(90,9,'vedette','checkbox','Vedette',0,1,1,1,1,1,'{}',7),(91,9,'titre','text','Titre',1,1,1,1,1,1,'{}',2),(92,9,'texte','rich_text_box','Texte',1,1,1,1,1,1,'{}',5),(93,9,'titre_secondaire','text','Titre Secondaire',0,1,1,1,1,1,'{}',4),(94,10,'id','text','Id',1,0,0,0,0,0,'{}',1),(95,10,'nom','text','Nom',1,1,1,1,1,1,'{}',2),(96,10,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(97,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(98,8,'projet_belongstomany_tag_relationship','relationship','tags',0,1,1,1,1,1,'{\"model\":\"App\\\\Http\\\\Models\\\\Tag\",\"table\":\"tags\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"nom\",\"pivot_table\":\"projet_tag\",\"pivot\":\"1\",\"taggable\":\"0\"}',10),(99,10,'tag_belongstomany_projet_relationship','relationship','projets',0,1,1,1,1,1,'{\"model\":\"App\\\\Http\\\\Models\\\\Projet\",\"table\":\"projets\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"titre\",\"pivot_table\":\"projet_tag\",\"pivot\":\"1\",\"taggable\":\"0\"}',5),(100,11,'id','text','Id',1,0,0,0,0,0,'{}',1),(101,11,'projet_id','number','Projet Id',1,1,1,1,1,1,'{}',2),(102,11,'tag_id','number','Tag Id',0,1,1,1,1,1,'{}',3),(103,13,'id','text','Id',1,0,0,0,0,0,'{}',1),(104,13,'nom','text','Nom',0,1,1,1,1,1,'{}',2),(105,13,'article_id','text','Article Id',0,1,1,1,1,1,'{}',3),(106,9,'article_belongsto_rubrique_relationship','relationship','rubriques',0,1,1,1,1,1,'{\"model\":\"App\\\\Http\\\\Models\\\\Rubrique\",\"table\":\"rubriques\",\"type\":\"belongsTo\",\"column\":\"rubrique_id\",\"key\":\"id\",\"label\":\"nom\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(107,9,'rubrique_id','text','Rubrique Id',0,1,1,1,1,1,'{}',10);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy',NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-02-01 06:35:18','2020-02-04 14:48:13'),(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-02-01 06:35:19','2020-02-03 10:57:20'),(8,'projets','projets','Projet','Projets',NULL,'App\\Http\\Models\\Projet',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-02-02 09:52:53','2020-02-07 07:38:42'),(9,'articles','articles','Article','Articles',NULL,'App\\Http\\Models\\Article',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-02-04 15:19:59','2020-02-08 09:59:30'),(10,'tags','tags','Tag','Tags',NULL,'App\\Http\\Models\\Tag',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-02-06 14:04:20','2020-02-07 08:20:53'),(11,'projet_tag','projet-tag','Projet Tag','Projet Tags',NULL,'App\\Http\\Models\\ProjetTag',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-02-07 07:21:39','2020-02-07 07:45:58'),(13,'rubriques','rubriques','Rubrique','Rubriques',NULL,'App\\Http\\Models\\Rubrique',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2020-02-08 09:39:17','2020-02-08 09:39:17');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2020-02-01 06:35:18','2020-02-01 06:35:18',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,10,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,11,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.settings.index',NULL),(11,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.categories.index',NULL),(12,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2020-02-01 06:35:18','2020-02-01 06:35:18','voyager.posts.index',NULL),(13,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2020-02-01 06:35:19','2020-02-01 06:35:19','voyager.pages.index',NULL),(14,1,'Hooks','','_self','voyager-hook',NULL,5,13,'2020-02-01 06:35:19','2020-02-01 06:35:19','voyager.hooks',NULL),(15,1,'Projets','','_self',NULL,NULL,NULL,15,'2020-02-02 09:52:53','2020-02-02 09:52:53','voyager.projets.index',NULL),(16,1,'Articles','','_self',NULL,NULL,NULL,16,'2020-02-04 15:19:59','2020-02-04 15:19:59','voyager.articles.index',NULL),(17,1,'Tags','','_self',NULL,NULL,NULL,17,'2020-02-06 14:04:20','2020-02-06 14:04:20','voyager.tags.index',NULL),(18,1,'Projet Tags','','_self',NULL,NULL,NULL,18,'2020-02-07 07:21:39','2020-02-07 07:21:39','voyager.projet-tag.index',NULL),(19,1,'Rubriques','','_self',NULL,NULL,NULL,19,'2020-02-08 09:39:17','2020-02-08 09:39:17','voyager.rubriques.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2020-02-01 06:35:18','2020-02-01 06:35:18');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1),(24,'2016_01_01_000000_create_pages_table',2),(25,'2016_01_01_000000_create_posts_table',2),(26,'2016_02_15_204651_create_categories_table',2),(27,'2017_04_11_000000_alter_post_nullable_fields_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `titre_menu` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sous_titre_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,1,'home','2020-02-01 06:35:19','2020-02-03 10:58:54','Je suis un développeur Front-End avec une expérience freelance dans la création de sites Web et d\'applications Web. Je me spécialise dans JavaScript et j\'ai une expérience personnelle avec C #. J\'ai également des connaissances en Game development avec Unity .\r\nJetez un œil à mon travail ou contactez-moi! en construction','home','Bienvenue sur mon super Portfolio',NULL,NULL,'pages\\February2020\\ZAxZeyoTA8Bmgltmxyqo.png'),(2,1,'portfolio','2020-02-01 10:34:25','2020-02-01 10:34:25','Donec elementum mi vitae enim fermentum lobortis. In hac habitasse platea dictumst. Ut pellentesque, orci sed mattis consequat, libero ante lacinia arcu, ac porta lacus urna in lorem. Praesent consectetur tristique augue, eget elementum diam suscipit id. Donec elementum mi vitae enim fermentum lobortis.','Portfolio','Our portfolio','We are leading Company','Specializing in Wordpress Theme Development',NULL),(3,1,'blog','2020-02-01 10:36:51','2020-02-01 10:36:51',NULL,'Blog','Our blog',NULL,NULL,NULL),(4,1,'contact','2020-02-01 10:38:26','2020-02-01 10:38:26','Address: 123 Fake Street, LN1 2ST, London, United Kingdom\r\nPhone: +44 123 654321\r\nFax: +44 123 654321\r\nEmail: getintoutch@yourcompanydomain.com','Contact','Contact Us','Get in touch with us',NULL,NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(2,'browse_bread',NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(3,'browse_database',NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(4,'browse_media',NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(5,'browse_compass',NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18'),(6,'browse_menus','menus','2020-02-01 06:35:18','2020-02-01 06:35:18'),(7,'read_menus','menus','2020-02-01 06:35:18','2020-02-01 06:35:18'),(8,'edit_menus','menus','2020-02-01 06:35:18','2020-02-01 06:35:18'),(9,'add_menus','menus','2020-02-01 06:35:18','2020-02-01 06:35:18'),(10,'delete_menus','menus','2020-02-01 06:35:18','2020-02-01 06:35:18'),(11,'browse_roles','roles','2020-02-01 06:35:18','2020-02-01 06:35:18'),(12,'read_roles','roles','2020-02-01 06:35:18','2020-02-01 06:35:18'),(13,'edit_roles','roles','2020-02-01 06:35:18','2020-02-01 06:35:18'),(14,'add_roles','roles','2020-02-01 06:35:18','2020-02-01 06:35:18'),(15,'delete_roles','roles','2020-02-01 06:35:18','2020-02-01 06:35:18'),(16,'browse_users','users','2020-02-01 06:35:18','2020-02-01 06:35:18'),(17,'read_users','users','2020-02-01 06:35:18','2020-02-01 06:35:18'),(18,'edit_users','users','2020-02-01 06:35:18','2020-02-01 06:35:18'),(19,'add_users','users','2020-02-01 06:35:18','2020-02-01 06:35:18'),(20,'delete_users','users','2020-02-01 06:35:18','2020-02-01 06:35:18'),(21,'browse_settings','settings','2020-02-01 06:35:18','2020-02-01 06:35:18'),(22,'read_settings','settings','2020-02-01 06:35:18','2020-02-01 06:35:18'),(23,'edit_settings','settings','2020-02-01 06:35:18','2020-02-01 06:35:18'),(24,'add_settings','settings','2020-02-01 06:35:18','2020-02-01 06:35:18'),(25,'delete_settings','settings','2020-02-01 06:35:18','2020-02-01 06:35:18'),(26,'browse_categories','categories','2020-02-01 06:35:18','2020-02-01 06:35:18'),(27,'read_categories','categories','2020-02-01 06:35:18','2020-02-01 06:35:18'),(28,'edit_categories','categories','2020-02-01 06:35:18','2020-02-01 06:35:18'),(29,'add_categories','categories','2020-02-01 06:35:18','2020-02-01 06:35:18'),(30,'delete_categories','categories','2020-02-01 06:35:18','2020-02-01 06:35:18'),(31,'browse_posts','posts','2020-02-01 06:35:18','2020-02-01 06:35:18'),(32,'read_posts','posts','2020-02-01 06:35:18','2020-02-01 06:35:18'),(33,'edit_posts','posts','2020-02-01 06:35:18','2020-02-01 06:35:18'),(34,'add_posts','posts','2020-02-01 06:35:18','2020-02-01 06:35:18'),(35,'delete_posts','posts','2020-02-01 06:35:18','2020-02-01 06:35:18'),(36,'browse_pages','pages','2020-02-01 06:35:19','2020-02-01 06:35:19'),(37,'read_pages','pages','2020-02-01 06:35:19','2020-02-01 06:35:19'),(38,'edit_pages','pages','2020-02-01 06:35:19','2020-02-01 06:35:19'),(39,'add_pages','pages','2020-02-01 06:35:19','2020-02-01 06:35:19'),(40,'delete_pages','pages','2020-02-01 06:35:19','2020-02-01 06:35:19'),(41,'browse_hooks',NULL,'2020-02-01 06:35:19','2020-02-01 06:35:19'),(42,'browse_projets','projets','2020-02-02 09:52:53','2020-02-02 09:52:53'),(43,'read_projets','projets','2020-02-02 09:52:53','2020-02-02 09:52:53'),(44,'edit_projets','projets','2020-02-02 09:52:53','2020-02-02 09:52:53'),(45,'add_projets','projets','2020-02-02 09:52:53','2020-02-02 09:52:53'),(46,'delete_projets','projets','2020-02-02 09:52:53','2020-02-02 09:52:53'),(47,'browse_articles','articles','2020-02-04 15:19:59','2020-02-04 15:19:59'),(48,'read_articles','articles','2020-02-04 15:19:59','2020-02-04 15:19:59'),(49,'edit_articles','articles','2020-02-04 15:19:59','2020-02-04 15:19:59'),(50,'add_articles','articles','2020-02-04 15:19:59','2020-02-04 15:19:59'),(51,'delete_articles','articles','2020-02-04 15:19:59','2020-02-04 15:19:59'),(52,'browse_tags','tags','2020-02-06 14:04:20','2020-02-06 14:04:20'),(53,'read_tags','tags','2020-02-06 14:04:20','2020-02-06 14:04:20'),(54,'edit_tags','tags','2020-02-06 14:04:20','2020-02-06 14:04:20'),(55,'add_tags','tags','2020-02-06 14:04:20','2020-02-06 14:04:20'),(56,'delete_tags','tags','2020-02-06 14:04:20','2020-02-06 14:04:20'),(57,'browse_projet_tag','projet_tag','2020-02-07 07:21:39','2020-02-07 07:21:39'),(58,'read_projet_tag','projet_tag','2020-02-07 07:21:39','2020-02-07 07:21:39'),(59,'edit_projet_tag','projet_tag','2020-02-07 07:21:39','2020-02-07 07:21:39'),(60,'add_projet_tag','projet_tag','2020-02-07 07:21:39','2020-02-07 07:21:39'),(61,'delete_projet_tag','projet_tag','2020-02-07 07:21:39','2020-02-07 07:21:39'),(62,'browse_rubriques','rubriques','2020-02-08 09:39:17','2020-02-08 09:39:17'),(63,'read_rubriques','rubriques','2020-02-08 09:39:17','2020-02-08 09:39:17'),(64,'edit_rubriques','rubriques','2020-02-08 09:39:17','2020-02-08 09:39:17'),(65,'add_rubriques','rubriques','2020-02-08 09:39:17','2020-02-08 09:39:17'),(66,'delete_rubriques','rubriques','2020-02-08 09:39:17','2020-02-08 09:39:17');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (5,1,1,'esse','ssss',NULL,'esse',0,'2020-02-05 15:29:25','2020-02-05 15:29:25','sssss');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projet_tag`
--

DROP TABLE IF EXISTS `projet_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projet_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projet_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projet_tag_projet_id_index` (`projet_id`),
  KEY `projet_tag_tag_id_index` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projet_tag`
--

LOCK TABLES `projet_tag` WRITE;
/*!40000 ALTER TABLE `projet_tag` DISABLE KEYS */;
INSERT INTO `projet_tag` VALUES (2,4,1,NULL,NULL),(3,4,3,NULL,NULL),(4,4,4,NULL,NULL),(5,3,3,NULL,NULL),(6,3,4,NULL,NULL),(7,2,1,NULL,NULL),(8,1,2,NULL,NULL),(9,1,3,NULL,NULL);
/*!40000 ALTER TABLE `projet_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projets`
--

DROP TABLE IF EXISTS `projets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `featured` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projets`
--

LOCK TABLES `projets` WRITE;
/*!40000 ALTER TABLE `projets` DISABLE KEYS */;
INSERT INTO `projets` VALUES (1,'Projet-1','projets\\February2020\\sl4NKpuc0JpNpNFgDqOM.jpg','you tube','2 ème vidéo tutoriel YouTube pour créer une animation avec un menu hamburger','2020-01-27','2020-02-02 10:05:00','2020-02-05 18:11:46',1),(2,'Projet-2','projets\\February2020\\G2BEtS2CaslwZhVGn9yu.jpg','Pascal Lacroix','Projet web dynamique pour Pascal Lacroix dans le cadre du cours de Projet Web Dynamique à l\'IEPS Fléron, une blog réalisé avec Laravel & Voyager.','2020-02-02','2020-02-02 10:08:00','2020-02-05 17:03:24',0),(3,'Projet-3','projets\\February2020\\iAJdeWADvxL8dx6yFcgY.jpg','Admin 4 You','Refonte du site Admin 4 You, prototype et intégration','2019-11-03','2020-02-02 10:10:00','2020-02-05 18:11:26',1),(4,'Projet-4','projets\\February2020\\ZrYo5uyBlYx2ouSz55ba.jpg','IEPS Fléron','Refonte du site assistant-vétérinaire.be, prototype intégration et tableau de bord Voyager','2020-03-03','2020-02-02 10:12:00','2020-02-05 18:10:45',1);
/*!40000 ALTER TABLE `projets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2020-02-01 06:35:18','2020-02-01 06:35:18'),(2,'user','Normal User','2020-02-01 06:35:18','2020-02-01 06:35:18');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubriques`
--

DROP TABLE IF EXISTS `rubriques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubriques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubriques`
--

LOCK TABLES `rubriques` WRITE;
/*!40000 ALTER TABLE `rubriques` DISABLE KEYS */;
INSERT INTO `rubriques` VALUES (1,'Développement',NULL,'2020-02-08 09:55:34','2020-02-08 09:55:34'),(2,'Design',NULL,'2020-02-08 09:55:44','2020-02-08 09:55:44'),(3,'Framework',NULL,'2020-02-08 09:57:49','2020-02-08 09:57:49'),(4,'Programmes',NULL,'2020-02-08 09:58:00','2020-02-08 09:58:00'),(5,'Tendances',NULL,'2020-02-08 09:58:23','2020-02-08 09:58:23');
/*!40000 ALTER TABLE `rubriques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Back-end','2020-02-07 07:39:30','2020-02-07 07:39:30'),(2,'front-end','2020-02-07 08:17:29','2020-02-07 08:17:29'),(3,'Ui-Design','2020-02-07 08:21:12','2020-02-07 08:21:12'),(4,'Ux-Design','2020-02-07 08:21:20','2020-02-07 08:21:20'),(5,'Language','2020-02-07 08:21:48','2020-02-07 08:21:48');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2020-02-01 06:35:19','2020-02-01 06:35:19'),(2,'data_types','display_name_singular',6,'pt','Página','2020-02-01 06:35:19','2020-02-01 06:35:19'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2020-02-01 06:35:19','2020-02-01 06:35:19'),(4,'data_types','display_name_singular',4,'pt','Categoria','2020-02-01 06:35:19','2020-02-01 06:35:19'),(5,'data_types','display_name_singular',2,'pt','Menu','2020-02-01 06:35:19','2020-02-01 06:35:19'),(6,'data_types','display_name_singular',3,'pt','Função','2020-02-01 06:35:19','2020-02-01 06:35:19'),(7,'data_types','display_name_plural',5,'pt','Posts','2020-02-01 06:35:19','2020-02-01 06:35:19'),(8,'data_types','display_name_plural',6,'pt','Páginas','2020-02-01 06:35:19','2020-02-01 06:35:19'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2020-02-01 06:35:19','2020-02-01 06:35:19'),(10,'data_types','display_name_plural',4,'pt','Categorias','2020-02-01 06:35:19','2020-02-01 06:35:19'),(11,'data_types','display_name_plural',2,'pt','Menus','2020-02-01 06:35:19','2020-02-01 06:35:19'),(12,'data_types','display_name_plural',3,'pt','Funções','2020-02-01 06:35:19','2020-02-01 06:35:19'),(13,'categories','slug',1,'pt','categoria-1','2020-02-01 06:35:19','2020-02-01 06:35:19'),(14,'categories','name',1,'pt','Categoria 1','2020-02-01 06:35:19','2020-02-01 06:35:19'),(15,'categories','slug',2,'pt','categoria-2','2020-02-01 06:35:19','2020-02-01 06:35:19'),(16,'categories','name',2,'pt','Categoria 2','2020-02-01 06:35:19','2020-02-01 06:35:19'),(17,'pages','title',1,'pt','Olá Mundo','2020-02-01 06:35:19','2020-02-01 06:35:19'),(18,'pages','slug',1,'pt','ola-mundo','2020-02-01 06:35:19','2020-02-01 06:35:19'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2020-02-01 06:35:19','2020-02-01 06:35:19'),(20,'menu_items','title',1,'pt','Painel de Controle','2020-02-01 06:35:19','2020-02-01 06:35:19'),(21,'menu_items','title',2,'pt','Media','2020-02-01 06:35:19','2020-02-01 06:35:19'),(22,'menu_items','title',12,'pt','Publicações','2020-02-01 06:35:19','2020-02-01 06:35:19'),(23,'menu_items','title',3,'pt','Utilizadores','2020-02-01 06:35:19','2020-02-01 06:35:19'),(24,'menu_items','title',11,'pt','Categorias','2020-02-01 06:35:19','2020-02-01 06:35:19'),(25,'menu_items','title',13,'pt','Páginas','2020-02-01 06:35:19','2020-02-01 06:35:19'),(26,'menu_items','title',4,'pt','Funções','2020-02-01 06:35:19','2020-02-01 06:35:19'),(27,'menu_items','title',5,'pt','Ferramentas','2020-02-01 06:35:19','2020-02-01 06:35:19'),(28,'menu_items','title',6,'pt','Menus','2020-02-01 06:35:19','2020-02-01 06:35:19'),(29,'menu_items','title',7,'pt','Base de dados','2020-02-01 06:35:19','2020-02-01 06:35:19'),(30,'menu_items','title',10,'pt','Configurações','2020-02-01 06:35:19','2020-02-01 06:35:19');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/default.png',NULL,'$2y$10$Dcg3V09bersDLdNZz3yA0.PCRim8uUP/H7USTMYZTZ.2yIisn8XoS','9cWbDWhRE7F8jWdNTLm9yOUDJOLblrvUKFIgqN08Ib2QJX1y28zYanGFuw6s',NULL,'2020-02-01 06:35:18','2020-02-01 06:35:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-08 12:10:20
