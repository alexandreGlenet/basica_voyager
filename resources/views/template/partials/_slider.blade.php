<section id="main-slider" class="no-margin">

    <div class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1"></li>
            <li data-target="#main-slider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          {{-- SLIDE DE LA PAGE D ACCUEIL --}}
            <div class="item active" style="background-image: url('{{ Voyager::Image($page->photo) }}">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="carousel-content centered">
                                <h2 class="animation animated-item-1">{{ $page->titre }}</h2>
                                <p class="animation animated-item-2">{{ $page->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item--> {{-- FIN DU SLIDE DE LA PAGE D ACCUEIL --}}
            {{-- LES 2 AUTRES SLIDES - A PRIORI 2 PROJETS --}}
            @foreach ($projets->reverse() as $projet)
               {{-- Autre possibilités pour ne prendre que les projets mis en vedette avec le IF --}}
              {{-- @if ($projet->featured == 1) --}}
                <div class="item" style="background-image: url('{{ Voyager::Image($projet->photo) }}">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="animation animated-item-1">Powerful and Responsive HTML Template</h2>
                                    <p class="animation animated-item-2">Phasellus adipiscing felis a dictum dictum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at ligula risus. </p>
                                    <br>
                                    <a class="btn btn-md animation animated-item-3" href="{{ URL::route('projets.show', ['id' => $projet->id, 'slug' => Str::slug($projet->titre, '-')] )}}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item--> {{-- FIN DES 2 AUTRES SLIDES --}}
              {{-- @endif --}}


            @endforeach


        </div><!--/.carousel-inner-->
    </div><!--/.carousel-->
    <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
        <i class="icon-angle-left"></i>
    </a>
    <a class="next hidden-xs" href="#main-slider" data-slide="next">
        <i class="icon-angle-right"></i>
    </a>
</section><!--/#main-slider-->
