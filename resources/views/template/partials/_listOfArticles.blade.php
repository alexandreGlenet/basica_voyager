<div class="section">
<div class="container">
<div class="row">
  @foreach ($articles as $article)
    <!-- Blog Post Excerpt -->
    <div class="col-sm-6">
      <div class="blog-post blog-single-post">
        <div class="single-post-title">
          <h2>{{ $article->titre }}</h2>
        </div>

        <div class="single-post-image">
          <img src="{{ Voyager::image($article->thumbnail('cropped', 'photo')) }}" alt="{{ $article->titre }}">
        </div>

        <div class="single-post-info">
          <i class="glyphicon glyphicon-time"></i>15 OCT, 2014 <a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
        </div>

        <div class="single-post-content">
          <p>
            {!! Str::words($article->texte, '50', '...') !!}
          </p>
        <a href="{{ URL::route('articles.show', ['id' => $article->id, 'slug' => Str::slug($article->titre, '-')] )}}" class="btn">Read more</a>
        </div>
      </div>
    </div>
    <!-- End Blog Post Excerpt -->
  @endforeach



        <!-- Pagination -->
        <div class="pagination-wrapper ">
          <ul class="pagination pagination-sm">
            <li class="disabled"><a href="#">Previous</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">Next</a></li>
          </ul>
        </div>

</div>
</div>
</div>
