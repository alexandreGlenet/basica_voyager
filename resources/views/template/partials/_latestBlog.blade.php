<!-- Our Articles -->
<div class="section">
  <div class="container">
    <div class="row">
      <!-- Featured News -->
      <div class="col-sm-6 featured-news">
        <h2>Latest Blog Posts</h2>
        @foreach ($articles as $article)
          <div class="row">
            <div class="col-xs-4"><a href="{{ URL::route('articles.show', ['id' => $article->id, 'slug' => Str::slug($article->titre, '-')] )}}">
              <img src="{{ Voyager::image($article->thumbnail('cropped', 'photo')) }}" alt="Post Title"></a></div>
            <div class="col-xs-8">
              <div class="caption">
                <a href="{{ URL::route('articles.show', ['id' => $article->id, 'slug' => Str::slug($article->titre, '-')] )}}">{{ $article->titre }}</a>
              </div>
              <div class="date">14 August 2014 </div>
              <div class="intro">{!! Str::words($article->texte, '30', '...') !!}
                <a href="{{ URL::route('articles.show', ['id' => $article->id, 'slug' => Str::slug($article->titre, '-')] )}}">Read more...</a>
              </div>
            </div>
          </div>
        @endforeach


      </div>
      <!-- End Featured News -->


      <!-- Latest News FB -->
      <div class="col-sm-6 latest-news">
        <h2>Lastest Twitter News</h2>
        <div class="row">
          <div class="col-sm-12">
            <a class="twitter-timeline" data-width="505" data-height="450" href="https://twitter.com/AlexandreGlenet?ref_src=twsrc%5Etfw">Tweets by AlexandreGlenet</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>

      </div>
      <!-- End Featured News -->
    </div>
  </div>
</div>
