<!-- Our Portfolio -->

    <div class="section section-white">
      <div class="container">
        <div class="row">

    <div class="section-title">
    <h1>Our Recent Works</h1>
    </div>


  <ul class="grid cs-style-3">
    @foreach ($projets as $projet)
      <div class="col-md-4 col-sm-6">
    <figure>
      <img src="{{ Voyager::image($projet->thumbnail('cropped', 'photo')) }}" alt="{{ $projet->titre }}">
      <figcaption>
        <h3>{{ $projet->titre }}</h3>
        <span>{{ $projet->client }}</span>
        <a href="{{ URL::route('projets.show', ['id' => $projet->id, 'slug' => Str::slug($projet->titre, '-')] )}}">Take a look</a>
      </figcaption>
    </figure>
      </div>
    @endforeach

  </ul>
        </div>
      </div>
  </div>
<!-- Our Portfolio -->
