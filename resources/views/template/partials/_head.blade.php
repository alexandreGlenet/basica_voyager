<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>BASICA! A Free Bootstrap3 HTML5 CSS3 Template by Vactual Art</title>

<!-- Bootstrap Core CSS -->
<link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet">
{{-- <link href="{{ asset ('css/bootstrap.min.css') }}" rel="preload" as="style" onload ="this.rel = 'stylesheet'"> --}}

<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset ('css/main.css') }}">
<link href="{{ asset ('css/custom.css') }}" rel="stylesheet">



<!-- Custom Fonts & Icons -->
<link href="{{ asset ('http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800') }}" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset ('css/icomoon-social.css') }}">
<link rel="stylesheet" href="{{ asset ('css/font-awesome.min.css') }}">
