<div class="col-sm-4 blog-sidebar">

@include('template.partials._sideBarRecentPosts', ['articles' => \App\Http\Models\Article::orderBy('created_at', 'DESC')->take(4)->get()])
@include('template.partials._sideBarRubriques', ['rubriques' => \App\Http\Models\Rubrique::all()])

</div>
