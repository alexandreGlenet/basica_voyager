<!-- Page Title -->
<div class="section section-breadcrumbs">
<div class="container">
<div class="row">
  <div class="col-md-12">
    <h1>{{ $page->titre }}</h1>
  </div>
</div>
</div>
</div>


<div class="section">
<div class="container">
<div class="row">
<div class="col-sm-12">
    <h2>{{ $page->titre_description }}</h2>
    <h3>{{ $page->sous_titre_description }}</h3>
    <p>
      {{ $page->description }}
    </p>

  </div>
</div>
</div>
</div>
