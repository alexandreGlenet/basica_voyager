@extends('template.defaut')

@section('content1')






  <!-- Page Title -->
<div class="section section-breadcrumbs">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>{{ $projet->titre }}</h1>
    </div>
  </div>
</div>
</div>

  <div class="section">
  <div class="container">
    <div class="row">
      <!-- Product Image & Available Colors -->
      <div class="col-sm-6">
        <div class="product-image-large">
          <img src="{{ asset('storage/').'/'.$projet->photo }}" alt="{{ $projet->titre }}">
        </div>
        <div class="colors">
        <span class="color-white"></span>
        <span class="color-black"></span>
        <span class="color-blue"></span>
        <span class="color-orange"></span>
        <span class="color-green"></span>
      </div>
      </div>
      <!-- End Product Image & Available Colors -->
      <!-- Product Summary & Options -->
      <div class="col-sm-6 product-details">
        <h2>{{ $projet->titre }}</h2>
      <h3>Quick Overview</h3>
        <p>
          {{ $projet->description }}
      </p>
      <h3>Project Details</h3>
      <p><strong>Client: </strong>{{ $projet->client }}</p>
      <p><strong>Date: </strong>{{ \Carbon\Carbon::parse($projet->date)->format('d-M-Y') }}</p>
      <p><strong>Tags: </strong></p>
      @foreach ($projet->tags as $tag)
      <a href="{{ route('tags.show' , [ 'id' => $tag->id, 'slug' => Str::slug($tag->nom, '-')])}}" class="btn btn-default btn-xs">
        {{ $tag->nom }}</a>
      @endforeach
      </div>
      <!-- End Product Summary & Options -->

    </div>
</div>
</div>

@endsection
