@extends('template.defaut')

@section('content1')

  {{-- HOMEPAGE --}}
@if ($page->id === 1)

  @include('template.partials._slider', ['projets' => \App\Http\Models\Projet::where('featured', '=', '1')->get()]))
  @include('template.partials._ourRecentWork', ['projets' => \App\Http\Models\Projet::orderBy('created_at', 'DESC')->take(6)->get()])
  <hr>
  @include('template.partials._latestBlog', ['articles' => \App\Http\Models\Article::orderBy('created_at', 'DESC')->take(3)->get()])
  {{-- ENDHOMEPAGE --}}

  {{-- PORTFOLIO --}}
@elseif ($page->id === 2)
  @include('template.partials._pageTitleDescription')
  @include('template.partials._projetsList', ['projets' => \App\Http\Models\Projet::all()])
  {{-- ENDPORTFOLIO --}}

  {{-- BLOG --}}
@elseif ($page->id === 3)
  @include('template.partials._pageTitleDescription')
  @include('template.partials._listOfArticles', ['articles' => \App\Http\Models\Article::paginate(4)])
  {{-- ['articles' => \App\Http\Models\Article::paginate(4)]) --}}
  {{-- ENDBLOG --}}


@else

@endif

@endsection
