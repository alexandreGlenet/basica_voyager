<ul class="nav navbar-nav navbar-right">
  @foreach ($pages as $page)
    <li class="">
      <a href="{{ URL::route('pages.show', ['id' => $page->id, 'slug' => $page->slug] )}}">{{ $page->titre_menu }}</a>
    </li>
  @endforeach
</ul>
