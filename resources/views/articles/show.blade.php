@extends('template.defaut')

@section('content1')

  <!-- Page Title -->
<div class="section section-breadcrumbs">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>{{ $article->titre_principal }}</h1>
    </div>
  </div>
</div>
</div>

  <div class="section">
  <div class="container">
  <div class="row">
    <!-- Blog Post -->
    <div class="col-sm-8">
      <div class="blog-post blog-single-post">
        <div class="single-post-title">
          <h2>{{ $article->titre }}</h2>
        </div>

        <div class="single-post-image">
          <img src="{{ asset('storage/').'/'.$article->photo }}" alt="{{ $article->titre }}">
        </div>
        <div class="single-post-info">
          <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($article->date)->format('d M,Y') }}<a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
        </div>
        <div class="single-post-content">
          <h3>{{ $article->titre_secondaire }}</h3>
          <p>
            {!! $article->texte !!}
          </p>
        </div>
      </div>
    </div>
    <!-- End Blog Post -->
    <!-- Sidebar -->
    @include('template.partials._sideBar')
    <!-- End Sidebar -->
  </div>
</div>
</div>

@endsection
