@extends('template.defaut')

@section('content1')

  <!-- Page Title -->
<div class="section section-breadcrumbs">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>{{ $tag->nom }}</h1>
    </div>
  </div>
</div>
</div>

<div class="section">
<div class="container">
<div class="row">
<div class="col-sm-12">
    <h2>Voici les projets portant le tag {{ $tag->nom }}</h2>


  </div>
</div>
</div>
</div>

@include('template.partials._tagProjetsList')

@endsection
