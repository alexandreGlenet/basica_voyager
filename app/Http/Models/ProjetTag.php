<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class ProjetTag extends Model {

     // Modèle de le table pivot ('projet_tag') entre projet & tag

     protected $table = 'projet_tag';
   }
