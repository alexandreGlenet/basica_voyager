<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;
   use TCG\Voyager\Traits\Resizable;

   class Article extends Model {

     /**
      * [La table associée au modèle]
      * @var [string]
      */

     protected $table = 'articles';
     use Resizable;

     public function rubriques()
       {
         return $this->belongsTo('App\Http\Models\Rubrique');
       }
   }
