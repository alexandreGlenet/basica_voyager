<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;
   use TCG\Voyager\Traits\Resizable;

   class Projet extends Model {

     /**
      * [La table associée au modèle]
      * @var [string]
      */

     protected $table = 'projets';
     use Resizable;

     public function tags()
       {
         return $this->belongsToMany('App\Http\Models\Tag');
       }

   }
