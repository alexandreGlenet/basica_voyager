<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Tag extends Model {

     /**
      * [La table associée au modèle]
      * @var [string]
      */

     protected $table = 'tags';

     public function projets()
       {
         return $this->belongsToMany('App\Http\Models\Projet');
       }

   }
