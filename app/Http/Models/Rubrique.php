<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Rubrique extends Model {

     /**
      * [La table associée au modèle]
      * @var [string]
      */

     protected $table = 'rubriques';

     public function articles()
       {
         return $this->hasMany('App\Http\Models\Article');
       }

   }
