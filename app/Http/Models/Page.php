<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Page extends Model {

     /**
      * [La table associée au modèle]
      * @var [string]
      */

     protected $table = 'pages';
   }
