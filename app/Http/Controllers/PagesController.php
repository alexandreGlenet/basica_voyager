<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Page;



class PagesController extends Controller
{
  /**
   * [show description]
   * @param  int    $id     [l'id de la page]
   * @param  string $slug   [slug du titre]
   * @return [View]         [Vue Pages/show.blade.php]
   */

    public function show(int $id = 1, string $slug = null)
    {
      $page = Page::find($id);
      return View::make('pages.show', compact('page'));

    }

}
