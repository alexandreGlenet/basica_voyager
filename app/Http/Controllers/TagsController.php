<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Tag;



class TagsController extends Controller
{


  public function show(int $id = 1, string $slug = null)
  {
    $tag = Tag::find($id);
    return View::make('tags.show', compact('tag'));
  }

}
