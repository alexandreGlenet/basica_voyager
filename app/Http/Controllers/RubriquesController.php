<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Rubrique;



class RubriquesController extends Controller
{


  public function show(int $id = 1, string $slug = null)
  {
    $rubrique = Rubrique::find($id);
    return View::make('rubriques.show', compact('rubrique'));
  }

}
