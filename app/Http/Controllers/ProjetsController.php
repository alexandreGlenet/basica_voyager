<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Projet;



class ProjetsController extends Controller
{
  /**
   * [show description]
   * @param  int    $id     [l'id de la créature]
   * @param  string $slug   [slug du nom]
   * @return [View]         [Vue Crétures/show.blade.php]
   */

    public function show(int $id = 1, string $slug = null)
    {
      $projet = Projet::find($id);
      return View::make('projets.show', compact('projet'));
    }

    public function search()
    {

    }
    

}
